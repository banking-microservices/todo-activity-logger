package com.sapient.learning.activitylogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoActivityLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoActivityLoggerApplication.class, args);
	}

}
