package com.sapient.learning.activitylogger.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import com.sapient.learning.activitylogger.enums.EventType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document( indexName = "todo_activity_log" )
public class TaskEvent {
	
	@Id
	private String eventId;
	private EventType eventType;
	private String taskId;
	private String taskTitle;
	private LocalDateTime timestamp;
	
	private String username;

}
