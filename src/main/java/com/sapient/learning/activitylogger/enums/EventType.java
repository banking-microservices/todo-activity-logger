package com.sapient.learning.activitylogger.enums;

public enum EventType {
	
	TASK_CREATED,
	TASK_UPDATED,
	TASK_DELETED;

}
