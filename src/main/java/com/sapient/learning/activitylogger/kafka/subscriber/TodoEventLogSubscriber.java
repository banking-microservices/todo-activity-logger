package com.sapient.learning.activitylogger.kafka.subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.sapient.learning.activitylogger.entity.TaskEvent;
import com.sapient.learning.activitylogger.repository.TaskEventRepository;

@Component
public class TodoEventLogSubscriber {
	
	
	@Autowired
	private TaskEventRepository taskEventRepository;
	
	
	@KafkaListener( topics = "TASK_ACTIVITY_LOG", groupId = "todo_activity_log_subscribers", properties= {"spring.json.value.default.type=com.sapient.learning.activitylogger.entity.TaskEvent"} )
	public void eventLogListener( @Payload TaskEvent event ) {
		var save = taskEventRepository.save(event);
		
		System.out.println("Data Saved in Elasicsearch ::: " + save.toString());
	}

}
