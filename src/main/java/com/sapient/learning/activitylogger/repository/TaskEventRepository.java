package com.sapient.learning.activitylogger.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.sapient.learning.activitylogger.entity.TaskEvent;

@Repository
public interface TaskEventRepository extends ElasticsearchRepository<TaskEvent, String>  {

}
